# 1 Menampilkan Use Case Table untuk produk digital

A. UseCase User
| UseCase  | NilaiPrioritas |
| ---------| -------------- |
|  Melakukan pembayaran | 10 |
|  Melihat detail produk | 9 |
|  Melihat daftar produk | 9 |
|  Melakukan pelacakan pesanan | 8 |
|  Mencari produk | 8 |
|  Menambahkan produk ke keranjang | 7 |
|  Mengelola keranjang belanja | 6 |


B. UseCase Admin
| UseCase  | NilaiPrioritas |
| ---------| -------------- |
|  Mengelola katalog produk | 9 |
|  Mengelola pesanan        | 9 |
|  Membuat laporan | 8 |
|  Mengelola pengguna | 7 |


C. UseCase Manajemen data   (dashboard)
| UseCase  | NilaiPrioritas |
| ---------| -------------- |
|  Menghasilkan laporan | 9 |
|  Mengelola data pesanan | 9 |
|  Mengelola data produk | 8 |
|  Mengelola data pengguna | 8 |
|  Mengelola data transaksi | 7 |
|  Mengelola data pelanggan | 7 |
|  Mengelola data kategori | 6 |

# 2 Web Service (CRUD) dari produk digital

![CRUD](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/main/CRUD.gif)

# 3 Koneksi Ke database

![Koneksi Database](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/main/koneksidatabase.gif)

# 4 Visualisasi data untuk business intelligence produk digital

![](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/main/datavisualisasi1.gif)

![](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/main/datavisualisasi2.gif)

# 5 Built-in funtion dengan ketentuan
# 6 Penggunaan Subquery pada produk digital
# 7 Penggunaan Transaction pada produk digital
# 8 Penggunaan Procedure / Function dan Trigger pada produk digital
# 9 Demonstrasi Data Control Language (DCL) pada produk digital
# 10 Constraint yang digunakan pada produk digital
- Indexes
![](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/main/Indexs.gif)

- Foreign Key
![](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/main/constraints.gif)


# 11 Demonstrasi produk digital (Youtube)
# 12 Demonstrasi UI untuk CRUDnya

![](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/main/displaydemo.gif)
![](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/main/userdemo.gif)
![](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/main/admindemo.gif)
