# NO 1
1. Pertama kita unduh file instalasi PostgreSQl pada link ini https://www.enterprisedb.com/downloads/postgres-postgresql-downloads,  jalankan file tersebut nanti tampilan awalnya seperti ini, lalu klik next

![1](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Instalasi/1.png)

2. Pilih lokasi file untuk instal PostgreSQl, lalu klik next

![2](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Instalasi/2.png)

3. Pilih komponen yang ingin diinstal, lalu klik next

![3](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Instalasi/3.png)

4. Pilih lokasi file untuk store data, lalu klik next

![4](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Instalasi/4.png)

5. Buat password untuk database, password bebas dan disarankan yang mudah diingat, lalu klik next

![5](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Instalasi/5.png)

6. Pilih port number yang akan digunakan oleh server, lalu klik next

![6](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Instalasi/6.png)

7. Pilih lokasi, lalu klik next

![7](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Instalasi/7.png)

8. Disini muncul konfirmasi instalasi, lalu klik next

![8](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Instalasi/8.png)

9. Setup instalasi sudah siap untuk diinstal, lalu klik next

![9](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Instalasi/9.png)

10. Tunggu proses instalasi selesai

![10](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Instalasi/10.png)

11. Dan PostgreSQl telah terinstal di komputer atau laptop

![11](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Instalasi/11.png)


# NO 2
1. Buka pgAdmin4 yang dapat di cari di start, tampilan awalnya seperti ini

![1](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Konfigurasi/1.png)

2. Masukan password yang tadi dibuat, lalu tekan ok

![2](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Konfigurasi/2.png)

3. Buka server dan masukan kembali passwordnya, klik ok

![3](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Konfigurasi/3.png)

4. Nanti tampilan akan seperti ini

![4](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Konfigurasi/4.png)

5. Buka DBeaver, klik lambang colokan di kiri atas

![5](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Konfigurasi/5.png)

6. Lalu pilih PostgreSQl

![6](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Konfigurasi/6.png)

7. Sesuaikan port yang tadi sudah dimasukan, dan juga masukan password, lalu klik test connection, jika anda belum menginstal driver maka instal terlebih dahulu

![7](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Konfigurasi/7.png)

8. Setelah muncul tampilan seperti ini, maka sudah connect dan klik ok

![8](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Konfigurasi/8.png)

# NO 3
![hsl](https://gitlab.com/Mokyra18/praktikum-basis-data/-/raw/feature/Hasil.png)
